FROM node:5.11

WORKDIR /usr/src/app

RUN apt-get update -y && apt-get install net-tools -y

# Copy package .json & install dependencies
COPY package.json ./
COPY npm-shrinkwrap.json ./
RUN npm install
COPY . .

# Running the project & exposing the desired Port
EXPOSE 3200 3100
CMD [ "npm", "start" ]