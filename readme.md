# (React-Webpack)

## A starter project for react with webpack

### Prerequisits:

- node 5.11.0
- npm 3.8.6

### Installation:

For the convenience of development run following commands to install webpack & webpack-devserver globally on you machine.

```javascript
npm install -g webpack@1.13.2
npm install -g webpack-dev-server@1.16.2
```

Now it is time to install our application dependencies. Run following commands to your terminal:

```javascript
npm install
```

### Running the project:

Once all the dependencies are installed, issue following command to run the project:

```javascript
npm start
```

This will bootup up a server on http://localhost:3100

Enjoy playing........
