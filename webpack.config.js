var path = require('path');
var webpack = require('webpack');
var ProgressBarPlugin = require('progress-bar-webpack-plugin');
const PKG = require('./package.json');
var BrowserSyncPlugin = require('browser-sync-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');


module.exports = {
    /*
    Configuring development server
    */
    devServer: {
        host: 'localhost',
        inline: true,
        port: 3100
    },

    /*
    Application Entry point & output directory
    */
    entry: {
        app: './src/app.js',
        // vendor: ['react', 'react-dom', 'react-router', 'history', 'lodash', 'jquery']
        vendor: Object.keys(PKG.dependencies)
    },
    output: {
        path: __dirname + '/dist',
        filename: './script/[name].[hash].js'
    },

    resolve: {
        root: path.resolve(__dirname),
        alias: {
            App: 'src',
            System: 'src/system/System',
            react: 'node_modules/react', //  Adding this line for avoiding any conflict with same name issue
            materialize: 'node_modules/materialize-css/dist'
        },
        extensions: ['', '.js', '.jsx']
    },

    /*
    Configuring loaders
    */
    module: {
        loaders: [
            {
                test: /\.js/,
                loader: 'babel',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'react'],
                    plugins: ["transform-decorators-legacy", "transform-decorators", "transform-class-properties"]
                }
            },
            {
                test: /\.css|.scss/,
                loaders: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /.jpe?g|png/,
                loaders: [
                    'file?hash=sha512&digest=hex&name=assets/images/[hash].[ext]',
                    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false&name=assets/images/[hash].[ext]'
                ]
            },
            {
                test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=application/font-woff&name=assets/fonts/[hash].[ext]'
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=application/octet-stream&name=assets/fonts/[hash].[ext]'
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file?name=assets/fonts/[hash].[ext]'
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=image/svg+xml&name=assets/images/[hash].[ext]'
            }
        ]
    },

    /*
    Source map for debugging    
    */
    devtool: 'source-map',  //  cheap-module-eval-source-map

    /*
    Adding plugins
    */
    plugins: [
        new HtmlWebpackPlugin({
            title: PKG.name,
            filename: 'index.html',
            template: 'index.ejs'
        }),
        new ProgressBarPlugin(),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),
        new webpack.optimize.CommonsChunkPlugin({ name: "vendor" }),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new BrowserSyncPlugin({
            host: 'localhost',
            port: 3200,
            proxy: 'http://localhost:3100',
        }),
        new CleanWebpackPlugin(['dist'], {
            root: path.resolve(__dirname),
            verbose: true,
            dry: false            
        }),

        /*
        *   Uncomment following plugins for production environment
        */
        // new webpack.DefinePlugin({
        //     'process.env': {
        //         NODE_ENV: JSON.stringify( 'production' )
        //     }
        // }),
        // new webpack.optimize.UglifyJsPlugin(
        //     {
        //         compress: {
        //             warnings: false,
        //             drop_console: true
        //         }
        //     }
        // )        
    ]
};