import React from "react";
import { Router, Route, useRouterHistory, browserHistory   } from 'react-router';
import { createHashHistory } from 'history';

// Custom Components
import Home from "./route_components/home/home";
import Movies from "./route_components/movies/movies";
import Articles from "./route_components/articles/Articles";
import AlbumDetails from "./route_components/articles/AlbumDetails";
import RuleWriter from './route_components/rule_writer/RuleWriter'
import QueryBuilder from './route_components/query_builder/QueryBuilder'
import My404 from "./route_components/My404";

//  Configuring Routes
const appHistory = useRouterHistory(createHashHistory)({ 
	queryKey: false 
});

const routes = (
    <Router history={appHistory}>
      {/*<Route path="/" component={QueryBuilder}/>      */}
      <Route path="/" component={Home} />    
      <Route path="articles" component={Articles}/>
      <Route path="album/:article_id" component={AlbumDetails}/>
      <Route path="movies" component={Movies}/>
      <Route path="rule_writer" component={RuleWriter}/>      
      <Route path="query_builder" component={QueryBuilder}/>      
      <Route path='*' component={My404} />
    </Router>
); 

export {routes};