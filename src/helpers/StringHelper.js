String.prototype.spaceConcat = function( val ){
    if( null == val || "" == val ) return this;
    return this.trim() + ' ' + val;
}

String.prototype.valueConcat = function( val ){
    if( null == val || "" == val) return this;

    //  For Float value
    if( !isNaN(val) && val.toString().indexOf('.') != -1 )
        return this.trim() + ' ' + val;

    // for Int value
    else if( !isNaN(val) )
        return this.trim() + ' ' + val;

    return this.trim() + ' ' + '\'' + val + '\'';
}

String.prototype.actionConcat = function( val ){
    if( null == val || "" == val ) return this;
    return this.trim() + ' ' + 'console.log('+ val +');';
}