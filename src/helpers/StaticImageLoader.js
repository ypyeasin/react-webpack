class StaticImageLoader{

    load(){
        return {
            YUNA: require( 'App/images/yuna.jpg' ),
            PAGE_NOT_FOUND: require( 'App/images/page-not-found.png' )
        }
    }

}

export default new StaticImageLoader();