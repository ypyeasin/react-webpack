import React from "react";
import {RouteComponent, HTTP} from 'System';
import StaticImageLoader from 'App/helpers/StaticImageLoader';

export default class My404 extends RouteComponent{

	constructor(props){
		super(props);				
	}	

	view(){		
		return (
			<div className="_404 row">					
				<img src={StaticImageLoader.load().PAGE_NOT_FOUND} className="horizontal-center-align responsive-img absolute-center" />
			</div>		
		)
	}

}