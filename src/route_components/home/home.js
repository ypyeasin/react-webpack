import ReactDOM from "react-dom";
import React from "react";
import {RouteComponent, PreloaderHelper, ModalHelper, CONFIG} from 'System';

import LoginComponent from 'App/components/LoginComponent/LoginComponent';

export default class Home extends RouteComponent{	

	constructor(props){
		super(props);		
	}

	clicked(){
		alert("Clicked");
	}

	modalBody(){
		return (			
			<LoginComponent />			
		)
	}	

	view(){
		return (
			<div className="_homeComponent">
				<h3>Home</h3>	
				
				<div className="row">
					<div className="col s12 m12 l6">
						<p className="flow-text">
							Fusce fermentum odio nec arcu. Praesent nec nisl a purus blandit viverra. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Quisque id mi. Pellentesque posuere.
						</p>
					</div>			
					<div className="col s12 m12 l6">
						<LoginComponent />						
					</div>			
				</div>	
				
				<div className="row">
					<div className="col m12">
						<ModalHelper headerText="Login" modalSize={CONFIG.MODAL.SMALL} modalBody={this.modalBody()} />
					</div>
				</div>

			</div>
		)
	}

}