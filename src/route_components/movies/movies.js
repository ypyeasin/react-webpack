import React from "react";
import {RouteComponent, HTTP, PreloaderHelper, ImageHelper} from 'System';
import StaticImageLoader from 'App/helpers/StaticImageLoader';
import './style.scss';

export default class Movies extends RouteComponent{	

	constructor(props){
		super(props);		
		this.fetchMovies();		
	}	

	setInitState(){
		this.state = {
			isLoading: true,
			movies: [],
			tempMovies: []
		};
	}

	fetchMovies(){
		// var url = "https://jsonplaceholder.typicode.com/posts?time=" + Date.now();
		var url = "https://jsonplaceholder.typicode.com/posts";
		HTTP.get(url).then(response => {

			response.map( (a,b) => {
				var randomizer = new Date().getTime() + b;
				a.thumbnail = "//unsplash.it/200?image="+ b +"&" + randomizer
			})

			this.setState({movies: response, tempMovies: response, isLoading: false });
		});		
	}	

	moviesList(){

		if( this.state.movies.length ==0 && this.state.isLoading == false )
			return (<h6>No movies found</h6>);

		var image = StaticImageLoader.load().YUNA;
		return (
			<div className="row">				
				{this.state.movies.map( (movie, iterator) => {
					return (
						<div className="col s12 l4 inline-block-item animated fadeIn" key={iterator}>
							<div className="card-panel grey lighten-5 z-depth-1">
								<div className="row valign-wrapper">
									<div className="col s4 l4">
										<img src={image} alt={movie.title} className="circle responsive-img" />
										{/*<ImageHelper src={movie.thumbnail} alt={movie.title} className="circle responsive-img width110" />*/}
									</div>
									<div className="col s8 l8 min-height-70">
										<span className="black-text">
											{movie.title}
										</span>
									</div>
								</div>
							</div>
						</div>
					)
				})}
			</div>
		);
	}

	showMovies(){
		return (
			<div>
				<div className="row">
					<div className="col s12 l6">
						<h4>Movies</h4>
					</div>
					<div className="col s12 l6">
						{this.filterMovies()}
					</div>	
				</div>						
				{this.moviesList()}
				
				<PreloaderHelper visibility={this.state.isLoading} />
			</div>
		);
	}

	filterOut(e){
		var filteredMovies = this.state.tempMovies.filter(movie => {			
			 if( movie.title.match(e.target.value) ){
				return  movie;
			 }			 	
		});			
		this.setState({movies: filteredMovies});		
	}

	filterMovies(){
		return (			
			<div className="input-field col s12">
				<input id="last_name" type="text" className="validate" onChange={this.filterOut.bind(this)} />
				<label htmlFor="last_name">Filter Movie</label>
			</div>
		)
	}

	view(){
		return (
			<div className="_movies">
				<h3 className="component_header">Movies</h3>				
				{this.showMovies()}
			</div>
		)
	}

}