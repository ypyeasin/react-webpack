import ReactDOM from "react-dom";
import React from "react";
import {RouteComponent, PreloaderHelper} from 'System';
import {Link} from 'react-router'
import './style.scss';

export default class Articles extends RouteComponent{    

	constructor(props){
		super(props);
	}

    getArticles(){
        return [
            {id: 1, name: "Yeasin Hossain", description: "lorem ipsum"},
            {id: 2, name: "Yeasin Hossain", description: "lorem ipsum"},            
            {id: 3, name: "Yeasin Hossain", description: "lorem ipsum"},
            {id: 4, name: "Yeasin Hossain", description: "lorem ipsum"},
            {id: 5, name: "Yeasin Hossain", description: "lorem ipsum"},
            {id: 6, name: "Yeasin Hossain", description: "lorem ipsum"},
            {id: 7, name: "Yeasin Hossain", description: "lorem ipsum"},
            {id: 8, name: "Yeasin Hossain", description: "lorem ipsum"},
            {id: 9, name: "Yeasin Hossain", description: "lorem ipsum"},
            {id: 10, name: "Yeasin Hossain", description: "lorem ipsum"},
            {id: 11, name: "Yeasin Hossain", description: "lorem ipsum"},
            {id: 12, name: "Yeasin Hossain", description: "lorem ipsum"},
            {id: 13, name: "Yeasin Hossain", description: "lorem ipsum"},
        ]
    }

    itemClick(key, value){
        console.log(key, value);
    }

    getArticleList(){
        var articles = this.getArticles();
        var this$ = this;
        return articles.map(function(value, key){
            return <li key={key} onClick={this$.itemClick}>
                <Link to={`album/${value.id}`}>{value.name}</Link>
            </li>
        });
    }    

	view(){
		return (
			<div className="_articles">
				<h3 className="component_header">Articles</h3>
                <ul>
                    {this.getArticleList()}                    
                </ul>

                <div className="magicDiv"></div>
			</div>
		)
	}

}