import React from "react";
import {Link} from 'react-router'
import {RouteComponent, PreloaderHelper, HTTP, LightboxHelper, decorateMethod} from 'System';
import {Button} from 'react-materialize'

export default class AlbumDetails extends RouteComponent{

	PAGINATE = 20;	

	constructor(props){		
		super(props);				
		var g = this.fetchAlbumImages().then(res=> { console.log(res) });		
	}	

	@decorateMethod
	setInitState(){
		this.state = ({
			currentPage: 0,
			isFetching: true,			
			albumImages: [],
			albumAllImages: [],				
		});		

		return 1;
	}
	
	onComponentMount(){
		$('.carousel.carousel-slider').carousel({full_width: true, time_constant: 300});
		setInterval(() => $('.carousel').carousel('next'), 5000);
	}

	fetchAlbumImages(){
		const AlbumID = this.props.params.article_id;
		const API = "https://jsonplaceholder.typicode.com/photos";		
		return HTTP.get( API ).then( res => {			
			
			res.map( (a, b) => {
				var randomizer = new Date().getTime() + b;
				a.thumbnail = "//unsplash.it/200?image="+ b +"&" + randomizer
				a.src = "//unsplash.it/1920/1080/?image="+ b +"&" + randomizer;
			});

			this.setState({ isFetching: false, albumImages: res.getRange(0, this.PAGINATE), albumAllImages: res });
			return res;
		});
	}	

	getenrateCaroselView(){		
		return (
			<div className="carousel carousel-slider">				
				<a className="carousel-item" href="#one!"><img src="http://lorempixel.com/1024/350/city/7/"/></a>
				<a className="carousel-item" href="#two!"><img src="http://lorempixel.com/1024/350/city/5/"/></a>
				<a className="carousel-item" href="#three!"><img src="http://lorempixel.com/1024/350/city/9/"/></a>
				<a className="carousel-item" href="#four!"><img src="http://lorempixel.com/1024/350/city/3/"/></a>
			</div>
		)
	}

	generateGridView(){
		return (			
			<div className="margin-top-10">
				<PreloaderHelper visibility={this.state.isFetching} />
				<div className="row">					
					{this.state.albumImages.map( (item, iterator) => {
						return (	
							<div key={iterator} className="col s6 m4 l3">								
								<LightboxHelper src={item.thumbnail} alt={item.title} srcCollection={this.state.albumImages} />									
							</div>																
						)
					})}
				</div>
			</div>			
		)
	}

	seeMore(){
		const startFrom = (this.state.currentPage + 1) * this.PAGINATE;
		const data = this.state.albumAllImages.getRange( startFrom, this.PAGINATE );
		var newDataSet = this.state.albumImages.merge(data);
		this.setState({albumImages: newDataSet});
		this.setState({currentPage: this.state.currentPage + 1});		
	}

	toggleSeeMoreBtn(){
		if( !this.state.isFetching )
			return (
					<div className="row center-align">
						<Button onClick={this.seeMore.bind(this)} waves='light' >See more</Button>
					</div>
				);
	}	

	view(){
		return (
			<div className="_articles">				
                <div className="row margin-top-10">					
					<div className="col s12">
						{this.getenrateCaroselView()}
                    	{this.generateGridView()}
						{this.toggleSeeMoreBtn()}
                	</div>                
                </div>                
			</div>
		)
	}

}