import React from "react";
import {Link} from 'react-router'
import {RouteComponent, PreloaderHelper, HTTP, LightboxHelper, decorateMethod, ModalHelper, CONFIG} from 'System';
import {Button, Dropdown, NavItem, Input} from 'react-materialize';

require('App/helpers/StringHelper');
require('./style.scss');

export default class QueryBuilder extends RouteComponent{	

	static TableData = [
		{id: 1, name: 'Elvin', item: 'Eclair', price: 6, quantity: 2},
		{id: 2, name: 'Alan', item: 'Jelly Bean', price: 1, quantity: 4},
		{id: 3, name: 'John', item: 'Pineapple', price: 3, quantity: 8},
		{id: 4, name: 'Alex', item: 'Juice', price: 2, quantity: 10},
		{id: 5, name: 'Haque', item: 'Intel', price: 700, quantity: 12},
		{id: 6, name: 'Jeffry', item: 'MAC', price: 1200, quantity: 88},
		{id: 7, name: 'William', item: 'Lollipop', price: 1.5, quantity: 63},
		{id: 8, name: 'Tomy', item: 'Nut', price: 0.5, quantity: 84},
		{id: 9, name: 'Jesus', item: 'Ice Cream', price: 5, quantity: 32},
		{id: 10, name: 'Schofield', item: 'Sandwitch', price: 2, quantity: 45}
	];

	static suggestions = [
		{id: 1, name: 'symptom', 'dataType': 'string', type: 'param'},
		{id: 2, name: 'age', 'dataType': 'integer', type: 'param'},
		{id: 3, name: 'sugar', 'dataType': 'float', type: 'param'},
		{id: 4, name: 'LDL', 'dataType': 'float', type: 'param'},
		{id: 5, name: 'HDL', 'dataType': 'float', type: 'param'},				
		{id: 6, name: '&', 'dataType': 'string', type: 'joiner'},
		{id: 7, name: '|', 'dataType': 'string', type: 'joiner'},
		{id: 8, name: '>', 'dataType': 'string', type: 'comparator'},
		{id: 9, name: '<', 'dataType': 'string', type: 'comparator'},
		{id: 10, name: '<=', 'dataType': 'string', type: 'comparator'},
		{id: 11, name: '>=', 'dataType': 'string', type: 'comparator'},
		{id: 12, name: '=', 'dataType': 'string', type: 'comparator'},
		{id: 13, name: 'Gender', 'dataType': 'string', type: 'param'},
		{id: 14, name: 'RBC_count', 'dataType': 'integer', type: 'param'},
		{id: 15, name: 'Createnin', 'dataType': 'integer', type: 'param'},
		{id: 16, name: 'Bilirubin', 'dataType': 'integer', type: 'param'}
	];

	static tooltipMessage = {
		0: 'Invalid Param name',
		1: 'Inavlid comparator. Expecting a valid comparator (>, <, =, >=, <=)',
		2: 'Expecting a valid value',
		3: 'Expecting a valid joiner (& or |)'
	};

	constructor(param){
        super(param);        
    }	

	setInitState(){
		this.state = {
			name: '',
			value: '',			
			isValidQuery: true, 
			items: QueryBuilder.TableData,
			suggestions: []
		};
	}

	componentWillMount(){
		this.handleChange = this.handleChange.bind(this);
		this.handleNameChange = this.handleNameChange.bind(this);
		this.saveExpression = this.saveExpression.bind(this);
	}

	view(){
		return (
			<div className="_query-builder">	
				<div className="row margin-top-10">
					{this.showQueryBuilder()}
				</div>
                <div className="row margin-top-10">					
					<div className="col s12">
						{/* this.showTable() */}
                	</div>                					
                </div>                
			</div>
		)
	}

	/*
	*	Internal Components
	*/
	showSuggestion(){
		if( !this.state.value ) return "";

		var cleanedValue = this.state.value.replace(/\s+/gi, " ");
		var splittedValue = cleanedValue.split(" ");

		var sequence = (splittedValue.length-1)%4;
		var typeSequence = ['param', 'comparator', 'value', 'joiner'];
		var type = typeSequence[sequence];
		console.log('-------', type, '-------');


		const expr = new RegExp( splittedValue[splittedValue.length-1], 'gi' );
		var generatedSuggestions = QueryBuilder.suggestions.filter((item) => {			
			if( null != item.name.match(expr) && type == item.type )
				return item;
		});

		var suggestedList = generatedSuggestions.map((item) => {
			return (
				<li key={item.id} className="suggestion-list-item pointer">{item.name}</li>
			);
		});		

		return (
			<div className="row">
				<ul className="suggestion-list">
					{suggestedList}
				</ul>
			</div>
		);
	}
	showQueryBuilder(){
		var errorIcon = this.state.isValidQuery ? "hide" : "";
		return (
			<div className="col m12 s12">
				<div className="row">
					<Input type="text" label="Name" s={12} onChange={this.handleNameChange} />
				</div>
				<div className="row relative">					
					{/*<input type="text" placeholder="Your query..." onChange={this.handleChange} />*/}
					<Input type="text" label="Expression" s={12} onChange={this.handleChange} />
					<span className={errorIcon + " error-icon"}>
						<i className="material-icons dp48 error">not_interested</i>
					</span>								
					{this.showSuggestion()}						
				</div>
				<div className="row">
					<div className="col s12 m12">
						<Button disabled={!this.state.isValidQuery || !this.state.name || !this.state.value} className="right" waves='light' onClick={this.saveExpression}>Save</Button>
					</div>					
				</div>			
				<div className="row">
					{this.generateExpectedStatement()}					
				</div>
			</div>
		);
	}
	showTable(){
		var tableRows = this.state.items.map((item) => {
			return (
				<tr key={item.id}>
					<td>{item.name}</td>
					<td>{item.item}</td>
					<td>{item.price}</td>
					<td>{item.quantity}</td>
				</tr>
			)
		});

		return (
			<table className="striped">
				<thead>
					<tr>
						<th>Name</th>
						<th>Item Name</th>
						<th>Item Price</th>
						<th>Quantity</th>
					</tr>
				</thead>
				<tbody>
					{tableRows}
				</tbody>
			</table>
		)
	}
	

	/*
	*	Internal methods
	*/
	_resetTableData(){
		this.setState({
			items: data
		});		
	}

	validateQuery( value ){
		var isValid = true;
		var cleanedValue = value.replace(/\s+/gi, " ");
		var splittedValue = cleanedValue.split(" ");
		splittedValue = _.filter(splittedValue, function(i){ if(i.trim() != '') return i });
		var iteration = 1;

		for(var i = 0; i < splittedValue.length; i++){
			if( splittedValue.length < 3 || (splittedValue.length > 3 && splittedValue.length%4 != 3) ){
				this.setState({isValidQuery: false});
				return;
			}

			if( iteration > 4 ) iteration = 1;

			switch(iteration){
				case 1:
					isValid = this.checkType( splittedValue[i], 'param' );
					break;
				case 2: 
					isValid = this.checkType( splittedValue[i], 'comparator' );
					break;
				case 3: 
					isValid = this.checkType( splittedValue[i], 'value' );
					break;
				case 4: 
					isValid = this.checkType( splittedValue[i], 'joiner' );
					break;
			}

			this.setState({isValidQuery: isValid});
			if( !isValid ) return;
			iteration++;
		}

	}

	checkType(value, type){
		var tempRegEx = null;

		if( type == 'value' ){
			return value == "" ? false : true;
		}
		
		var suggestionModel = _.find(QueryBuilder.suggestions, (item) => {			
			if( item.name.toLocaleLowerCase() == value && item.type == type ) return item;
		});

		return undefined == suggestionModel ? false : true;		
	}

	generateExpectedStatement( value ){
		var currentValue = this.state.value;
		if( "" == currentValue ) return "";

		var cleanedValue = currentValue.replace(/\s+/gi, " ");
		var splittedValue = cleanedValue.split(" ");
		var iteration = 0;
		var typeSequence = ['param', 'comparator', 'value', 'joiner'];
		var expectedStatement = "";


		for(var i = 0; i < splittedValue.length; i++ ){
			if( iteration > 3 ) iteration = 0;

			var matchedResult = _.find( QueryBuilder.suggestions, (item) => {
				if( item.type == typeSequence[iteration] && item.name == splittedValue[i] ) return item;
			});
			
			if( undefined == matchedResult && typeSequence[iteration] != 'value' ) 
				expectedStatement = expectedStatement.spaceConcat( '<span class="tooltipped error pointer" data-tooltip="'+QueryBuilder.tooltipMessage[iteration]+'">'+ splittedValue[i] + '</span>' );
			else
				expectedStatement = expectedStatement.spaceConcat( splittedValue[i] );

			iteration++;
		}

		this.activateToolTip();
		return (
			<blockquote>
				<h6>Generated Expression</h6>
				<div dangerouslySetInnerHTML={{__html: (expectedStatement)}}></div>
			</blockquote>
		);
	}

	activateToolTip(){		
		setTimeout(function(){
			$('.tooltipped').tooltip({delay: 50});
		}, 50);
	}

	saveExpression(){
		alert("Feature under development");
	}

	/*
	*	Event Handlers
	*/
	handleChange(event){
		this.setState({	value: event.target.value });		
		this.validateQuery(event.target.value);
	}

	handleNameChange(event){
		this.setState({name: event.target.value});
	}
} 