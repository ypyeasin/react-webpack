import React from "react";
import {Link} from 'react-router'
import {RouteComponent, PreloaderHelper, HTTP, LightboxHelper, decorateMethod, ModalHelper, CONFIG} from 'System';
import {Button, Dropdown, NavItem, Input} from 'react-materialize';
import VariableComponent from './VariableComponent/VariableComponent';

require('App/helpers/StringHelper');
require('./style.scss');

export default class RuleWriter extends RouteComponent{	

	static operator = [
		{id: 1, name: 'Equal To', 'symbol': '=='},
		{id: 2, name: 'Greater then', 'symbol': '>'},
		{id: 3, name: 'Greater or Equal To', 'symbol': '>='},
		{id: 4, name: 'Less then', 'symbol': '<'},
		{id: 5, name: 'Lesser or Equal To', 'symbol': '<='},
		{id: 6, name: 'Contains', 'symbol': 'in'},
	];	

	constructor(param){
        super(param);        
    }

	componentDidMount(){
		$('.tooltipped').tooltip({delay: 50});
	}

	setInitState(){
		this.state = {
			test: 'Yeasin',
			iterator: [
				{id: 1, condition: null, operator: null, key: "", value: "", action: "", enableAction: false}
			]
		};
	}

	view(){
		return (
			<div className="_ruleWriter">					
                <div className="row margin-top-10">					
					<div className="col m8 s12">						
						{this.ruleWriteEditor()}
                	</div>                
					<div className="col m4 s12">						
						{this.ruleBuildPReview()}
                	</div>                
                </div>                
			</div>
		)
	}

	/*
	*	Internal Methods
	*/
	ruleBuildPReview(){
		var Model = JSON.stringify( this.state.iterator );
		var STATEMENT = "";
		for(var i in this.state.iterator){
			STATEMENT = STATEMENT
					.spaceConcat( this.state.iterator[i].condition )
					.spaceConcat( this.state.iterator[i].key )
					.spaceConcat( this.getOperatorSybmolByID( this.state.iterator[i].operator ) )
					.valueConcat( this.state.iterator[i].value )					
					.actionConcat( this.state.iterator[i].action )
					.trim();
		}

		return (
			<div>
				<h6>Generated Code</h6>
				<blockquote>
					{STATEMENT}
				</blockquote>				
			</div>
		);
	}

	ruleWriteEditor(){
		var that = this;
		var temp = this.state.iterator.map(function(item){
			var conditionPrinter = function(){
				if( !!item.condition )
					return (
						<div className="row center">
							{item.condition}
						</div>
					);
			};

			var actionInput = (item) => {
				if( !!item.enableAction )
					return (
						<input type="text" name="action" placeholder="Action" onChange={that.handleChange.bind(that)} />
					);

				return (<input type="text" name="action" placeholder="Action" disabled onChange={that.handleChange.bind(that)} />);
			};

			return (
				<div key={item.id} className="row">					
					{conditionPrinter()}
					<div className="row">
						<div className="col m2">
							<input type="text" name="key" placeholder="Input Variable" onChange={that.handleChange.bind(that, item)} />
						</div>
						<div className="col m3 no-margin-top-input">
							{that.operatorOptions(item)}
						</div>
						<div className="col m3">
							<input type="text" name="value" placeholder="value" onChange={that.handleChange.bind(that, item)} />
						</div>
						<div className="col m3">
							{actionInput(item)}
						</div>
						<div className="col m1">
							<Dropdown trigger={ <span className=""><i className="material-icons pointer margin-top-10">more_vert</i></span> }>
								<NavItem onClick={that.toggleAction.bind(that, item)}>Action</NavItem>
								<NavItem divider />
								<NavItem onClick={that.addCondition.bind(that, '&&')}>AND</NavItem>
								<NavItem onClick={that.addCondition.bind(that, '||')}>OR</NavItem>
								<NavItem divider />
								<NavItem onClick={that.removeCondition.bind(that, item.id)}>Remove</NavItem>
							</Dropdown>
						</div>				
					</div>					
				</div>
			);
		});

		return (
			<div className="col m12">				
				<div className="row">					
					<ModalHelper trigger={<span data-target="someID"><i className="material-icons dp48 right tooltipped pointer" data-position="bottom" data-delay="50" data-tooltip="Add Variable">library_add</i></span>} 
						headerText="Create Variable" modalSize={CONFIG.MODAL.SMALL} modalBody={<VariableComponent />}  />
				</div>
				{temp}			
			</div>
		)
	}	

	addCondition(operator){		
		var length = this.state.iterator.push( {id: new Date().getUTCMilliseconds(), condition: operator} );

		if( length > 1 ){
			this.state.iterator[length-2]['enableAction'] = false;
			this.state.iterator[length-2]['action'] = "";
		}

		this.setState({
			iterator: this.state.iterator
		});
	}

	removeCondition(id){		
		_.remove(this.state.iterator, (i) => i.id == id);
		this.setState({iterator: this.state.iterator});
	}

	operatorOptions(model){
		const conditions = RuleWriter.operator.map(function(item){
			return (
				<option key={item.id} value={item.id}>{item.name}</option>
			)
		});

		return (
			<Input type='select' name="operator" defaultValue='0' onChange={this.handleChange.bind(this, model)}>
				<option value="0" disabled>Select Operator</option>
				{conditions}
			</Input>
		);
	}

	getOperatorSybmolByID( id ){
		if( null == id || 0 == id) return "";
		return _.find( RuleWriter.operator, {id: parseInt(id)} ).symbol;
	}

	toggleAction( model ){
		var action = !model.enableAction;
		var tempModel = {};
		var state = this.state.iterator.map((item) => {
			if( item.id == model.id ){
				tempModel['enableAction'] = action;
				return _.assign(item, tempModel);
			} 
			return item;
		});		
		this.setState({ iterator: state });
	}

	/*
	*	Event Handlers
	*/
	handleChange(model, value){
		var tempModel = {};
		var state = this.state.iterator.map((item) => {
			if( item.id == model.id ){
				tempModel[value.target.name] = value.target.value;
				return _.assign(item, tempModel);
			} 
			return item;
		});		
		this.setState({ iterator: state });
	}
}