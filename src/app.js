import React from "react";
import ReactDOM from "react-dom";
import { routes } from './routes';
import AuthenticationService from './services/AuthenticationService';

//  Importing Vendor libraries & styles globally
import $ from 'jquery';
import _ from 'lodash';

//  Loading styles
import './style/style.scss';
import './style/utils.scss';

// Loading global libraries to window scope
window.$ = window.jquery = window.jQuery= $;
window._ = _;


// Bootstrapping the React application
ReactDOM.render(routes, document.getElementById('content'));