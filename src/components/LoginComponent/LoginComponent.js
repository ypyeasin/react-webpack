import React from 'react'
import {Component} from 'System'
import { Button, Input } from 'react-materialize'
require('./style.scss');

export default class LoginComponent extends Component{        

    static FORM_TYPE = {
        SIGNIN: "signin",
        SIGNUP: "signup",
        FORGET: "forget"
    };

    constructor(param){
        super(param);
        // this.setInitState();

        // Binding context to handlers
        this.handleChange = this.handleChange.bind(this)
        this.signupHandler = this.signupHandler.bind(this);        
        this.signinHandler = this.signinHandler.bind(this);
        this.forgetHandler = this.forgetHandler.bind(this);         
    }

    setInitState(){
        this.state = {
            selectedForm: LoginComponent.FORM_TYPE.SIGNIN,
            signup: {},
            signin: {},
            forget: {}
        };
    }

    // Handlers
    setFormMode(form){        
        this.setState({selectedForm: form});
    }

    signupHandler(){
        console.log( this.state.signup );
    }

    signinHandler(){
        console.log( this.state.signin );
    }

    forgetHandler(){
        console.log( this.state.forget );
    }

    handleChange(e){
        var form = $( e.target ).parents('form').attr('id');
        var elem = e.target.name;
        this.state[form][elem] = e.target.value;
        console.log( e.target.value );
    }    

    // Sub components
    signUpForm(){
        return (
            <form id={LoginComponent.FORM_TYPE.SIGNUP} className="col s12" action="javascript:void(0)" onSubmit={this.signupHandler} noValidate>
                <div className="row">
                    <Input type="text" name="first_name" label="First Name" s={12} onChange={this.handleChange} defaultValue={this.state.signup.first_name}  />
                    <Input type="text" name="last_name" label="Last Name" s={12} onChange={this.handleChange} defaultValue={this.state.signup.last_name} />
                    <Input type="email" name="email" label="Email" s={12} onChange={this.handleChange} defaultValue={this.state.signup.email} />
                    <Input type="password" name="password" label="Password" s={12} onChange={this.handleChange} defaultValue={this.state.signup.password} />
                    <Input type="password" name="confirm_password" label="Confirm Password" s={12} onChange={this.handleChange} defaultValue={this.state.signup.confirm_password} />
                </div>                                   
                <div className="row">
                    <div className="col m6">                        
                        <Button waves='light'>Sign Up</Button>
                    </div>
                    <div className="col m6 account_links">
                        <a href="javascript:void(0)" onClick={this.setFormMode.bind(this, LoginComponent.FORM_TYPE.SIGNIN)}>Already have an account, Sign In</a>                                                                                                 
                    </div>
                </div>
            </form>
        )
    }

    signInForm(){
        const formID = LoginComponent.FORM_TYPE.SIGNIN + (new Date).getTime()
        return (            
            <form id={formID} className="col s12" action="javascript:void(0)" onSubmit={this.signinHandler} noValidate>                
                <div className="row">  
                    <div className="col s12">
                        <Input label="Email" s={12} name="email"  type="email" onChange={this.handleChange} defaultValue={this.state[LoginComponent.FORM_TYPE.SIGNIN]['email']} />                        
                        <Input label="Password" s={12} name="password" type="password" onChange={this.handleChange} defaultValue={this.state[LoginComponent.FORM_TYPE.SIGNIN]['password']} />
                        <Input label="Remember Me" defaultValue='checked' s={12} name="remember" type="checkbox" onChange={this.handleChange} className='filled-in' />
                    </div>                                    
                </div>                                    
                <div className="row">
                    <div className="input-field col s12 m6">                        
                        <Button waves='light'>Submit</Button>                                                                      
                    </div>
                    <div className="input-field col s12 m6 account_links hide-on-small-only">
                        <a href="javascript:void(0)" onClick={this.setFormMode.bind(this, LoginComponent.FORM_TYPE.SIGNUP)}>Don't have an account, Sign Up</a>
                        <a href="javascript:void(0)" onClick={this.setFormMode.bind(this, LoginComponent.FORM_TYPE.FORGET)}>Forgot Password</a>                                                                      
                    </div>
                    <div className="input-field col s12 m6 hide-on-med-and-up">
                        <a href="javascript:void(0)" onClick={this.setFormMode.bind(this, LoginComponent.FORM_TYPE.SIGNUP)}>Don't have an account, Sign Up</a><br/>
                        <a href="javascript:void(0)" onClick={this.setFormMode.bind(this, LoginComponent.FORM_TYPE.FORGET)}>Forgot Password</a>                                                                      
                    </div>
                </div>
            </form>                
        )
    }

    forgetPasswordForm(){
        return (
            <form id={LoginComponent.FORM_TYPE.FORGET} action="javascript:void(0)" onSubmit={this.forgetHandler} noValidate>
                <div className="row">   
                    <div className="col s12">                 
                        <Input type="email" name="email" label="Email" s={12} onChange={this.handleChange} defaultValue={this.state[LoginComponent.FORM_TYPE.FORGET]['email']} />                    
                    </div> 
                </div> 
                <div className="row">
                    <div className="input-field col m6">
                        <Button waves='light'>Submit</Button>                                                                      
                    </div> 
                    <div className="input-field col m6 account_links">
                        <a href="javascript:void(0)" onClick={this.setFormMode.bind(this, LoginComponent.FORM_TYPE.SIGNIN)}>Already have an account, Sign In</a>                                                                                              
                    </div>                   
                </div>
            </form>
        );
    }

    getForm(){
        if( this.state.selectedForm == LoginComponent.FORM_TYPE.SIGNUP )
            return this.signUpForm();
        else if( this.state.selectedForm == LoginComponent.FORM_TYPE.FORGET )
            return this.forgetPasswordForm();        
        return this.signInForm();
    }

    view(){
        return (
            <div className="_loginComponent row">                
                {this.getForm()}
            </div>
        )        
    }

} 