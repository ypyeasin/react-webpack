import React, {Component} from 'react';
import AssetLoader from 'System'
import 'App/style/vendors/fontawsome/css/font-awesome.css';

export default class RouteComponent extends React.Component{

  static justLanded = 0;

  constructor(params) {
  	super(params);    
    RouteComponent.justLanded++;
    console.log("Route component initialized");
    this.setInitState();

    //  Materialize asset needs to be here, otherwise velocity error occurs
    require('materialize-css/dist/js/materialize.js');
    require('materialize-css/dist/css/materialize.min.css');
  }  

  setInitState(){}
  
	componentWillMount(){}

  componentDidMount(){
    $(".button-collapse").sideNav({
      closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor      
    });

    this.onComponentMount();
  }

  onComponentMount(){}

	componentWillReceiveProps(){}

  shouldComponentUpdate(){ return true; }

  componentWillUpdate (){}

  componentDidUpdate(){}

  componentWillUnmount (){}

  header(){
		return (
      <div className="navbar-fixed-1">
        <nav>
          <div className="container">
            <div className="nav-wrapper">
              <a href="#/" className="brand-logo"><i className="tiny material-icons">cloud</i>Rule writer</a>
              <a href="#" data-activates="mobile-demo" className="button-collapse"><i className="material-icons">menu</i></a>                        
              <ul id="nav-mobile" className="right hide-on-med-and-down">
                <li><a href="#/">Home</a></li>
                <li><a href="#/articles">Articles</a></li>
                <li><a href="#/movies">Movies</a></li>
                <li><a href="#/rule_writer">Rule Writer</a></li>					
                <li><a href="#/query_builder">Query Builder</a></li>					
              </ul>
              <ul className="side-nav" id="mobile-demo">
                <li><a href="#/">Home</a></li>
                <li><a href="#/articles">Articles</a></li>
                <li><a href="#/movies">Movies</a></li>
                <li><a href="#/rule_writer">Rule Writer</a></li>					
                <li><a href="#/query_builder">Query Builder</a></li>					
              </ul>
            </div>
          </div>        
        </nav>
      </div>        
		);		
	}

  view(){}

  footer(){
    var year = new Date().getFullYear();
    return (
      <footer className="page-footer">
        <div className="container">
          <div className="row">
            <div className="col l6 s12">
              <h5 className="white-text">Footer Content</h5>
              <p className="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
            </div>
            <div className="col l4 offset-l2 s12">
              <h5 className="white-text">Links</h5>
              <ul>
                <li><a className="grey-text text-lighten-3" href="https://bitbucket.org/ypyeasin/react-webpack">Project Repo</a></li>                                
              </ul>
            </div>
          </div>
        </div>
        <div className="footer-copyright">
          <div className="container">
          © {year} Copyright Text
          <a className="grey-text text-lighten-4 right" href="#!">More Links</a>
          </div>
        </div>
      </footer>
    )
  }

  render(){    
    const aminationClassComponent = RouteComponent.justLanded == 1 ? "" : ""; // animated fadeIn
    const aminationClassContent = RouteComponent.justLanded == 1 ? "" : ""; // animated fadeIn

    return (      
      <div className={"_routeComponent " + aminationClassComponent}>
        {this.header()}
        <div className={"container full-height-70 " + aminationClassContent}>
          {this.view()}          
        </div>        
        {this.footer()}
      </div>
    )    
  }

}