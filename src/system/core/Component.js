import React from 'react';
import AssetLoader from 'System'
import 'App/style/vendors/fontawsome/css/font-awesome.css';

export default class Component extends React.Component{
  
  constructor(params) {    
  	super(params);    
    console.log("Component initialized");

    this.setInitState = this.setInitState.bind(this);
    this.setInitState();
  } 

  setInitState(){}
  
  componentWillMount(){}

  componentDidMount(){}

  componentWillReceiveProps(){}

  shouldComponentUpdate(){ return true; }

  componentWillUpdate (){}

  componentDidUpdate(){}

  componentWillUnmount (){} 

  view(){
      return (<h4>Your custom component</h4>)
  }

  render(){
    return (      
          <div className="_component">
            {this.view()}
          </div>
    )    
  }

}