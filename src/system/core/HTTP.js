import Request from 'axios';

class HTTP {

    constructor(){
        console.log("HTTP Initialized");
    }

    get(url){
        return Request.get(url).then(response => {
            return response.data;
        });
    }

    send(){
        console.log("Needs to be implemented for POST request");
    }

}

export default new HTTP(); 