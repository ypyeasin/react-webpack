import React from 'react'
import Component from '../core/Component'
import {CONFIG} from'./../Config'
import { Button } from 'react-materialize'

export default class ModalHelper extends Component{
    
    constructor(param){
        super(param);
        
        //  initializing public properties
        this._id = new Date().getTime();
        this._modal_size = this.props.modalSize || CONFIG.MODAL.SMALL;        
    }

    componentDidMount(){
        $('#' + this._id).modal();
    }

    triggerOn(){
        if( this.props.trigger ){
            if( !this.props.trigger.props['data-target'] ) throw 'You need to provide data-target for the trigger element';
            this._id = this.props.trigger.props['data-target'];
            return this.props.trigger;
        }            
        return <Button data-target={this._id} waves='light' className="btn">Modal</Button>;
    }

    view(){
        return (
            <div className="_modalHelper">                
                {this.triggerOn()}
                <div id={this._id} className={"modal " +  this._modal_size}>                    
                    <div className="modal-content">
                        <h4>{this.props.headerText}</h4>                        
                        {this.props.modalBody}
                    </div>
                    <div className="modal-footer">
                        <a href="javascript:void(0)" className=" modal-action modal-close waves-effect waves-green btn-flat"><i className="fa fa-times" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        )
    }

}