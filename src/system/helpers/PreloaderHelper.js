import React from 'react';
import Component from '../core/Component';

export default class PreloaderHelper extends Component{
        
    constructor(param){
        super(param);        
    }

    preloaderStyle(){
        return {
            margin: 'auto',
            width: '35px'
        };
    }

    preloader(){
        return (
            <div className="preloader-wrapper small active">
                <div className="spinner-layer spinner-green-only">
                    <div className="circle-clipper left">
                        <div className="circle"></div>
                    </div><div className="gap-patch">
                        <div className="circle"></div>
                    </div><div className="circle-clipper right">
                        <div className="circle"></div>
                    </div>
                </div>
            </div>            			
		); 
    }    

    view(){
        const style = this.preloaderStyle();
        return (            			
            <div style={style}>
                {this.props.visibility ? this.preloader() : ""}
            </div>			
		);
    }

}

