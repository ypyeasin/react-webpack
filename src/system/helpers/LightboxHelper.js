import React from 'react'
import Component from '../core/Component'
import {PreloaderHelper} from './PreloaderHelper'
import Lightbox from 'react-image-lightbox';
import {CONFIG} from'./../Config'

export default class LightboxHelper extends Component{    

    ID = new Date().getTime();    

    constructor(props){
        super(props);
    }

    componentDidMount(){}

    setInitState(){
        this.state = _.assignIn(this.prepareLightBoxState());                
    }

    prepareLightBoxState(){
        var photoIndex = _.findIndex(this.props.srcCollection, (item) => item.thumbnail == this.props.src);
        
        var result = {
            src: this.props.src || CONFIG.IMAGE.IMAGE_NOT_FOUND,
            isLoading: true,
            failed: false,
            photoIndex: photoIndex,
            isOpen: false,
            srcCollection: this.props.srcCollection
        };
        return result; 
    }

    imageLoaded(e){        
        $(e.target).removeClass('hide');
        this.setState({isLoading: false, failed: false});
    }

    imageFailed(e){                
        this.setState({isLoading: false, failed: true});
    }

    imageReload(e){
        const newSrc = this.state.src + "&time=" + new Date().getTime();
        this.setState({isLoading: true, failed: false, src: newSrc});        
    }

    lightBox(){
        const { photoIndex, isOpen, } = this.state;        
        return (
            <div>
                {isOpen &&
                    <Lightbox
                        mainSrc={this.state.srcCollection[photoIndex].src}
                        nextSrc={this.state.srcCollection[(photoIndex + 1) % this.state.srcCollection.length].src}
                        prevSrc={this.state.srcCollection[(photoIndex + this.state.srcCollection.length - 1) % this.state.srcCollection.length].src}

                        onCloseRequest={() => this.setState({ isOpen: false })}
                        onMovePrevRequest={() => this.setState({
                            photoIndex: (photoIndex + this.state.srcCollection.length - 1) % this.state.srcCollection.length,
                        })}
                        onMoveNextRequest={() => this.setState({
                            photoIndex: (photoIndex + 1) % this.state.srcCollection.length,
                        })}
                    />
                }
            </div>
        )
    }    

    toggleLoaderRefreshIcon(){
        if( this.state.isLoading )
            return <img src={CONFIG.IMAGE.ICON_LOADING} />
        else if( this.state.failed )
            return <i className="fa fa-refresh fa-2x absolute-center" onClick={this.imageReload.bind(this)} aria-hidden="true"></i>
    }

    view(){
        return (
            <div className="_LightboxHelper animated fadeIn absolute-center-horizontal">
                <div className="absolute icon_loader">
                    {this.toggleLoaderRefreshIcon()}
                </div>
                <img 
                    id={this.ID} 
                    src={this.state.src}
                    onLoad={ this.imageLoaded.bind(this) }  
                    onError={ this.imageFailed.bind(this) }
                    className="hide materialboxed width150 animated fadeIn"
                    onClick={() => this.setState({ isOpen: true })} />
                {this.lightBox()}                
            </div>
        );
    }

}