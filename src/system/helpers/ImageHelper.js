import React from 'react'
import Component from '../core/Component'
import {PreloaderHelper} from './PreloaderHelper'
import Lightbox from 'react-image-lightbox';
import {CONFIG} from'./../Config'

export default class ImageHelper extends Component{

    ID = new Date().getTime();    

    constructor(props){
        super(props);
    }    

    setInitState(){}

    imageLoaded(e){
        $(e.target).removeClass('hide');
        $(e.target).siblings().fadeOut();
    }   

    view(){
        console.log( this.props.className );
        return (
            <div className="_ImageHelper animated fadeIn">
                <div className="absolute icon_loader">
                    <img src={CONFIG.IMAGE.ICON_LOADING} className={this.props.className} />
                </div>
                <img 
                    {...this.props}
                    id={this.ID} 
                    src={this.props.src}
                    onLoad={this.imageLoaded}                     
                    className= {this.props.className + "hide"} />
            </div>
        );
    }

}