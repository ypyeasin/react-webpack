/*
*   Custom Array helper, returns an array based on given start & end limit
*/
Array.prototype.getRange = function(start = 0, limit = 0){
    var result = [];
    for(var i = start; i < (start + limit); i++) result.push( this[i] );
    return result;
}

Array.prototype.merge = function(newArray = []){
    
    for(var i in newArray){        
        if( !_.isFunction( newArray[i] ) ) 
            this.push( newArray[i] );
    }

    return this; 
}