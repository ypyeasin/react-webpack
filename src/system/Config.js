export const CONFIG = {
    
    //  Modal Configurations
    MODAL:{
        SMALL   : "_small-modal",
        MEDIUM  : "_medium-modal",
        LARGE   : "_large-modal"
    },

    //  Image Settings
    IMAGE: {
        ICON_LOADING    : require('./../images/three-dots.svg'),
        THUMB_LOADING   : require('./../images/three-dots.svg'),
        ICON_FALLBACK   : require('./../images/three-dots.svg'),
        THUMB_FALLBACK  : require('./../images/three-dots.svg'),
        IMAGE_NOT_FOUND : require('./../images/image-not-found.svg')
    }

}